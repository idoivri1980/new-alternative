var tracksMap, tracksMapMarkers = [], tracksMapPath;

Template.tracks.created = function() {
    this.subscribe('tracks', function () {
        _updateTracksMap();
        /*Tracker.afterFlush(function () {
            console.log('afterFlush');
        });*/
    });
    this.subscribe('users');
    this.subscribe('journey', Router.current().params._journeyId, function () {
        _updateTracksMap();
    });
};

var findTracks = function() {
    var userId = Router.current().params._userId;
    var journeyId = Router.current().params._journeyId;
    if (journeyId === 'all') {
        return Tracks.find({userId: userId}, {sort: {'location.timestamp': -1}});
    } else {
        return Tracks.find({userId: userId, journeyId: journeyId}, {sort: {'location.timestamp': -1}});        
    }
};

Template.tracks.helpers({
    tracks: function () {
        return findTracks();
    },
    title: function () {
        var userId = Router.current().params._userId;
        var journeyId = Router.current().params._journeyId;
        var user = Meteor.users.findOne({_id: userId});
        if (!user) return 'Loading...';
        var userName = user.profile.name;
        if (journeyId === 'all') {
            return 'All Tracks for ' + userName;
        } else {
            return 'Tracks for Journey #' + journeyId + ' by ' + userName;
        }
        return user.profile.name;
        return '';
    },
    showAll: function() {
        var journeyId = Router.current().params._journeyId;
        return journeyId === 'all';
    },
    journey: function () {
        var journeyId = Router.current().params._journeyId;
        if (journeyId !== 'all') {
            return Journeys.findOne({_id: journeyId});
        }
    },
    timestamp: function() {
        var timestamp = new Date(this.location.timestamp);
        var now = new Date();
        var diffMs = (now - timestamp); // milliseconds between now & Christmas
        var diffMins = Math.round(((diffMs / 1) / 1) / 60000); // minutes
        return diffMins + ' min. ago';
    }
});

Template.tracks.onRendered(function () {
    if (GoogleMaps.loaded()) {
        _prepareTracksMap();
    } else {
        window.onload = function () {
            _prepareTracksMap();
        };
    }    
});

var _prepareTracksMap = function () {
    var mapOptions = {
        zoom: 16,
        center: new google.maps.LatLng(TLV_UNIVERSITY_GEOCOORDS[0],TLV_UNIVERSITY_GEOCOORDS[1])
    };
    tracksMap = new google.maps.Map(document.getElementById("appmap"), mapOptions);
    _updateTracksMap();
};

var _updateTracksMap = function () {
    console.log('_updateTracksMap');
    if (!tracksMap) return;
    var myLatLng;
    
    // clear existing markers
    for (var i = 0; i < tracksMapMarkers.length; i++) {
        tracksMapMarkers[i].setMap(null);
    }
    tracksMapMarkers = [];
    var tracksMapPathCoords = [];
    
    var tracks = findTracks().fetch();
    tracks.forEach(function(track) {
        myLatLng = {lat: track.location.coords.latitude, lng: track.location.coords.longitude};
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: tracksMap
        });
        tracksMapMarkers.push(marker);
        tracksMapPathCoords.push(myLatLng);
    });
    tracksMap.setCenter(myLatLng);
    
    var journeyId = Router.current().params._journeyId;
    if (journeyId !== 'all') {
        var journey = Journeys.findOne({_id: journeyId});
        if (journey) {
            console.log('has journey');
            console.log(journey);
            //myLatLng = {lat: journey.origin.lat, lng: journey.origin.lng};
            var marker = new google.maps.Marker({
                position: journey.origin,
                label: 'A',
                map: tracksMap
            });
            tracksMapMarkers.push(marker);
            var marker = new google.maps.Marker({
                position: journey.destination,
                label: 'B',
                map: tracksMap
            });
            tracksMapMarkers.push(marker);
        }
    }
    
    if (tracksMapPath) tracksMapPath.setMap(null);
    tracksMapPath = new google.maps.Polyline({
        path: tracksMapPathCoords,
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2
    });
    tracksMapPath.setMap(tracksMap);
};