Template.trackingStatus.helpers({
    isTracking: function () {
        return GeoTracker.getEstimatedTrackingEnd() > new Date();
    },
    remainingTrackingMinutes: function () {
        return Math.floor((GeoTracker.getEstimatedTrackingEnd() - new Date()) / 60000);
    }
});