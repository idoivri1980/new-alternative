Template.userStatistics.helpers({
    sortedAlternatives: function() {
        var sortedAlternatives = [];
        var maxCount = 0;
        for (var key in this.alternatives) {
            var count = this.alternatives[key];
            maxCount = Math.max(count, maxCount);
        }
        for (var key in this.alternatives) {
            var count = this.alternatives[key];
            if (count > 0) {
                sortedAlternatives.push({name: key, count: count, displayPercent: Math.floor((count / maxCount) * 100)});
            }
        };
        return sortedAlternatives;
    },
    methodDisplay: function() {
        return transportationMethods[this.name].display;
    }
});