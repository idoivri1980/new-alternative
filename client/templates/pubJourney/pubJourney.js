Template.pubJourney.created = function () {
    Session.set('pubJourney', null);
    console.log(Router.current().params._id);
    Meteor.call('getPubJourney', Router.current().params._id, function (err, result) {
        if (err) {
            console.log(err);
            return;
        }
        Session.set('pubJourney', result);
    });
    //this.subscribe('journey', Router.current().params._id);  
};

Template.pubJourney.helpers({
    journey: function () {
        return Session.get('pubJourney');
    },
    formatAddress: function (address) {
        return address.replace(', Israel', '').replace(', ישראל', '');
    },
    transportationMethodDisplay: function () {
        return transportationMethods[this.chosenType].display;
    }
});