Template.emailLogin.helpers({
  error: function () {
    return Session.get('loginError');
  }
});

Template.emailLogin.onCreated(function() {
  Session.set('loginError', null);
});

Template.emailLogin.events({
  'submit form': function(evt) {
    evt.preventDefault();
    var email = $('#email').val();
    var password = $('#password').val();
    Session.set('loginError', null);
    Meteor.loginWithPassword(email, password, function(error) {
      if (error) {
        Session.set('loginError', error.reason);
      } else {
        Router.go('navigate');
      }
    });
  }
});