/* 
 * wrapper for cordova-background-geolocation
 */

GeoTracker = {
    _params: {
        journeyId: -99,
        userId: -99
    },
    _initialized: false,
    _estimatedTrackingEnd: new Date(0),
    init: function () {
        var bgGeo = window.BackgroundGeolocation;

        /**
        * This callback will be executed every time a geolocation is recorded in the background.
        */
        var callbackFn = function(location, taskId) {
            bgGeo.finish(taskId); // <-- execute #finish when your work in callbackFn is complete
        };

        var failureFn = function(error) {
            console.log('BackgroundGeoLocation error', error);
        };

        bgGeo.configure(callbackFn, failureFn, {
            // Geolocation config
            desiredAccuracy: 0,
            stationaryRadius: 1,
            distanceFilter: 1,
            // disableElasticity: false, // <-- [iOS] Default is 'false'.  Set true to disable speed-based distanceFilter elasticity
            locationUpdateInterval: 1000,
            minimumActivityRecognitionConfidence: 0,   // 0-100%.  Minimum activity-confidence for a state-change 
            fastestLocationUpdateInterval: 1000,
            activityRecognitionInterval: 5000,
            // stopDetectionDelay: 1,   // [iOS] delay x minutes before entering stop-detection mode
            // stopTimeout: 2,  // Stop-detection timeout minutes (wait x minutes to turn off tracking)
            activityType: 'AutomotiveNavigation', // Presumably, this affects ios GPS algorithm. 
            // TODO: set this when user chooses route?

            // Application config
            debug: false, // <-- enable this hear sounds for background-geolocation life-cycle.
            forceReloadOnLocationChange: false,  // <-- [Android] If the user closes the app **while location-tracking is started** , reboot app when a new location is recorded (WARNING: possibly distruptive to user) 
            forceReloadOnMotionChange: false,    // <-- [Android] If the user closes the app **while location-tracking is started** , reboot app when device changes stationary-state (stationary->moving or vice-versa) --WARNING: possibly distruptive to user) 
            forceReloadOnGeofence: false,        // <-- [Android] If the user closes the app **while location-tracking is started** , reboot app when a geofence crossing occurs --WARNING: possibly distruptive to user) 
            stopOnTerminate: false,              // <-- Don't stop tracking when user closes app.
            startOnBoot: false,                  // <-- [Android] Auto start background-service in headless mode when device is powered-up.

            // HTTP / SQLite config
            url: Meteor.absoluteUrl('api/tracks'),
            method: 'POST',
            //batchSync: true,       // <-- [Default: false] Set true to sync locations to server in a single HTTP request.
            //autoSync: true,         // <-- [Default: true] Set true to sync each location to server as it arrives.
            //maxDaysToPersist: 1,    // <-- Maximum days to persist a location in plugin's SQLite database when HTTP fails
            //headers: {
            //    "X-FOO": "bar"
            //},
            params: this._params
        });
        this._initialized = true;
    },
    start: function () {
        if (!this._initialized) return;
        window.BackgroundGeolocation.start();
    },
    stop: function () {
        if (!this._initialized) return;
        window.BackgroundGeolocation.stop();
    },
    setTrackingTime: function (minutes) {
        if (!this._initialized) return;
        window.BackgroundGeolocation.setConfig(function(){}, function(){}, {
            stopAfterElapsedMinutes: minutes
        });
        var estimatedTrackingEnd = new Date(new Date().getTime() + minutes*60000);
        Meteor.users.update({_id:Meteor.userId()}, { $set: {'profile.estimatedTrackingEnd': estimatedTrackingEnd} });
        this._estimatedTrackingEnd = estimatedTrackingEnd;
    },
    getEstimatedTrackingEnd: function () {
        return this._estimatedTrackingEnd;
    },
    setJourneyId: function (journeyId) {
        //console.log('GeoTracker :: set journeyId to ' + journeyId);
        this._params.journeyId = journeyId;
        this._updateParams();
        Meteor.users.update({_id:Meteor.userId()}, { $set: {'profile.journeyId': journeyId} });
    },
    setUserId: function (userId) {
        //console.log('GeoTracker :: set userId to ' + userId);
        this._params.userId = userId;
        this._updateParams();
    },
    updateFromUser: function (user) {
        if (user) {
            this.setUserId(user._id);
            if (user.profile && user.profile.journeyId) {
                //console.log('profile has journeyId; set to ' + user.profile.journeyId);
                this.setJourneyId(user.profile.journeyId);
            }
            if (user.profile && user.profile.estimatedTrackingEnd) {
                //console.log('profile has estimatedTrackingEnd; set to ' + user.profile.estimatedTrackingEnd);
                this._estimatedTrackingEnd = user.profile.estimatedTrackingEnd;
            }
        }
    },
    getState: function (callbackFn) {
        if (!this._initialized) return;
        return window.BackgroundGeolocation.getState(callbackFn);
    },
    _updateParams: function () {
        if (!this._initialized) return;
        window.BackgroundGeolocation.setConfig(function(){}, function(){}, {
            params: this._params
        });
    }
};