/**
 * Created by idoivri on 23/11/2015.
 */

/** Object oriented stuff **/




/***/
alternativeDirectionsService = null;
alternativeMatrixService = null;
driving = null;
biking = null;
transit = null;
telofun = null;
train = null;
walking = null;
taxi = null;

//giving the resultsDict a name will make it survive hot code pushes


Template.results.onCreated(function() {

});

Template.results.onRendered(function() {

    var _fromLocation = Session.get("currentFromLocation");
    var _toLocation = Session.get("currentToLocation");

    /*
      TODO: this is a problematic branching, need to find a better alternative for this (no pun intended)
      The reason it's here is that the results screen should not be present
     */
    if (!GoogleMaps.loaded() || Session.get("hasChosenRoute")) {
      Router.go("navigate");
    }
    else if (Session.get("recalculateResults")===false) {
      var results = Session.get("results");
      console.log("results - after back from map",results);
      Session.set("results",results);
      return;
    }
    else {


      //var results = Session.get("results");
      //if (results.length > 0) {
      //  return;
      //} //TODO: is this the right way to check if results already exist?

      alternativeMatrixService = new google.maps.DistanceMatrixService();
      alternativeDirectionsService = new google.maps.DirectionsService();
      var fromLatLng = new google.maps.LatLng(_fromLocation.lat, _fromLocation.lng);
      var toLatLng = new google.maps.LatLng(_toLocation.lat, _toLocation.lng);

      //var driving = new Driving([_fromLocation.address], [_toLocation.address], alternativeDirectionsService, alternativeMatrixService, resultsDict);
      driving = new Driving(fromLatLng, toLatLng, alternativeDirectionsService, alternativeMatrixService);
      driving.activateDirectionsService(drivingCallBack, fromLatLng, toLatLng);

      taxi = new Taxi(fromLatLng, toLatLng, alternativeDirectionsService, alternativeMatrixService);
      taxi.activateDirectionsService(taxiCallBack, fromLatLng, toLatLng);

      biking = new Biking(fromLatLng, toLatLng, alternativeDirectionsService, alternativeMatrixService);
      biking.activateDirectionsService(bikingCallBack, fromLatLng, toLatLng);

      walking = new Walking(fromLatLng, toLatLng, alternativeDirectionsService, alternativeMatrixService);
      walking.activateDirectionsService(walkingCallBack, fromLatLng, toLatLng);

      transit = new Bus(fromLatLng, toLatLng, alternativeDirectionsService, alternativeMatrixService);
      transit.activateDirectionsService(busCallBack, fromLatLng, toLatLng);

      Session.set("telofuncounter",0);
      Session.set("telofunresults",[]);

      telofun = new TelOFun(fromLatLng, toLatLng, alternativeDirectionsService, alternativeMatrixService);
      telofun.activateDirectionsService(telOFunWalkingCallBack, telOFunBikingCallBack, fromLatLng, toLatLng);

      Session.set("traincounter",0);
      Session.set("trainresults",[]);

      train = new Train(fromLatLng, toLatLng, alternativeDirectionsService, alternativeMatrixService);
      train.activateDirectionsService(trainTransitCallBack, trainStationTransitCallBack, fromLatLng, toLatLng);




    }

});

Tracker.autorun(function() {
  console.log("GOT TO TELOFUN AUTORUN");
  var counter = Session.get("telofuncounter");
  if (!telofun) return;
  //console.log("counter is:",counter);
  if (counter===3) {
    Tracker.nonreactive(function () {
      var results = Session.get("telofunresults");

      //console.log("final TEL O FUN RESULT OBJECT", results);
      var walkingDistance = 0;
      var bikingDistance = 0;
      var walkingDuration = 0;
      var bikingDuration =0;

      results.forEach(function (item) {
        if (item.type==="WALKING") {
          walkingDistance += item.distance;
          walkingDuration += item.duration;
        }
        else if (item.type === "BIKING") {
          bikingDistance += item.distance;
          bikingDuration += item.duration;
        }
      });


      var resultObj = {
        "key": "TEL-O-FUN",
        "type": "TEL-O-FUN",
        "price": Math.floor(telofun.priceMultiplier*0.76), //TODO: calculate price
        "calories": Math.floor(telofun.bikingCaloriesMultiplier*bikingDistance + telofun.walkingCaloriesMultiplier*bikingDistance),
        "emissions": Math.floor(telofun.emissionsMultiplier*bikingDistance),
        "duration": Math.floor((walkingDistance + bikingDuration)/60)
      }

      var results = Session.get("results");
      results.push(resultObj);
      Session.set("results",results);
    });

  }
});


var sorters = {
    'eco': function(a, b) {
        return a.emissions - b.emissions;
    },
    'fit': function(a, b) {
        return b.calories - a.calories;
    },
    'cheap': function(a, b) {
        return a.price - b.price;
    },
    'fast': function(a, b) {
        return a.duration - b.duration;
    }
};

Template.results.helpers({
    results: function() {
      var results = Session.get("results");
      var sorter = sorters[Session.get("preference")];
      if (sorter) {
          results = results.sort(sorter);
      }
      return results;
    },
    methodDisplay: function() {
      return transportationMethods[this.key].display;
    },
    activePrefName: function() {
        return Session.get("preference");
    },
    durationFormatted: function() {
        var hours = Math.floor(this.duration / 60);
        var minutes = this.duration % 60;
        var result = '';
        if (hours > 0) {
            result += hours + ' hour';
            if (hours > 1) {
                result += 's';
            }
            result += ' ';
        }
        if (minutes > 0) {
            result += minutes + ' min.';
        }
        return result;
    }
});

Template.results.events({
    "click .btn-tomap": function(evt) {
      evt.preventDefault();

      var transportationKey = jQuery(evt.currentTarget).attr('data-trans-key');
      Session.set("draw_transportation", transportationKey);

      Router.go("map");
    }
});

var drivingCallBack = function (response, status) {
  console.log("GOT TO DISTANCE MATRIX CALLBACK: BIKING");
  if (status != 'OK') {
    console.log("Callback of Distance matrix return with error", response);
    return;
  }

console.log(driving);
console.log(driving.key);
  resultsToDraw[driving.key].push(response);

  //this.responseData = response.rows[0].elements[0];

  let responseData = response.routes[0].legs[0];
  var resultObj = {
    "key": driving.key,
    "type": driving.type,
    "price": Math.floor(driving.priceMultiplier*responseData.distance.value),
    "calories": Math.floor(driving.caloriesMultiplier*responseData.distance.value),
    "emissions": Math.floor(driving.emissionsMultiplier*responseData.distance.value),
    "duration": Math.floor(responseData.duration.value / 60)
  }

  var results = Session.get("results");
  results.push(resultObj);
  Session.set("results",results);

}

var taxiCallBack = function (response, status) {
  console.log("GOT TO DISTANCE MATRIX CALLBACK: TAXI");
  if (status != 'OK') {
    console.log("Callback of Distance matrix return with error", response);
    return;
  }

  resultsToDraw[taxi.key].push(response);

  //this.responseData = response.rows[0].elements[0];

  let responseData = response.routes[0].legs[0];

  var resultObj = {
    "key": taxi.key,
    "type": taxi.type,
    "price": Math.floor(getTaxiPrice(responseData.distance.value, responseData.duration.value)),
    "calories": Math.floor(taxi.caloriesMultiplier*responseData.distance.value),
    "emissions": Math.floor(taxi.emissionsMultiplier*responseData.distance.value),
    "duration": Math.floor(responseData.duration.value / 60)
  }

  var results = Session.get("results");
  results.push(resultObj);
  Session.set("results",results);
}

var bikingCallBack = function (response, status) {
  console.log("GOT TO DISTANCE MATRIX CALLBACK: BIKING");
  if (status != 'OK') {
    console.log("Callback of Distance matrix return with error", response);
    return;
  }

  resultsToDraw[biking.key].push(response);
  //console.log("biking result:", response);

  let responseData = response.routes[0].legs[0];
  var resultObj = {
    "key": biking.key,
    "type": biking.type,
    "price": Math.floor(biking.priceMultiplier*responseData.distance.value),
    "calories": Math.floor(biking.caloriesMultiplier*responseData.distance.value),
    "emissions": Math.floor(biking.emissionsMultiplier*responseData.distance.value),
    "duration": Math.floor(responseData.duration.value / 60 / 3)
  }

  var results = Session.get("results");
  results.push(resultObj);
  Session.set("results",results);

}


var walkingCallBack = function (response, status) {
  console.log("GOT TO DISTANCE MATRIX CALLBACK: WALKING");
  if (status != 'OK') {
    console.log("Callback of Distance matrix return with error", response);
    return;
  }

  resultsToDraw[walking.key].push(response);
  //console.log("walking result:", response);

  walking.response = response;
  let responseData = response.routes[0].legs[0];

  if (responseData.duration.value/60 > 90) {
    console.log("Walking duration is too long ("+(responseData.duration.value/60).toFixed(2)+") minutes, skipping walking result");
    return;
  }
  var resultObj = {
    "key": walking.key,
    "type": walking.type,
    "price": Math.floor(walking.priceMultiplier*responseData.distance.value),
    "calories": Math.floor(walking.caloriesMultiplier*responseData.duration.value),
    "emissions": Math.floor(walking.emissionsMultiplier*responseData.distance.value),
    "duration": Math.floor(responseData.duration.value / 60)
  }

  var results = Session.get("results");
  results.push(resultObj);
  Session.set("results",results);

}


var busCallBack = function (response, status) {
  console.log("GOT TO DISTANCE MATRIX CALLBACK: TRANSIT");
  if (status != 'OK') {
    console.log("Callback of Distance matrix return with error", response);
    return;
  }

  resultsToDraw[transit.key].push(response);
  //console.log("response - bus", response);

  var leg = response.routes[0].legs[0];
  var steps = leg.steps;

  //for each step filter the instructions to get the actual transportation method for each legs
  var travelMods = steps.filter(function(step){
      return step.instructions;
    })
    .map(function(step){
      return step.instructions.split(' ')[0];
    });
  //console.log("response: ",response);
  //console.log("travelMods",travelMods);
  //if transit result contains only walking - don't display
  if (travelMods.indexOf('Tram') == -1 && travelMods.indexOf('Bus') == -1 && travelMods.indexOf("אוטובוס") == -1) {
    console.log("No bus or tram in transit results!");
    return;
  }
  //if transit result's next bus is more than 90 minutes' waiting time - don't display
  var now = new Date();
  if (((leg.departure_time.value - now)/60000) > 90) {
    console.log("Departure time for transit is too far from current time - skipping result");
    return;
  }

  var isTram = travelMods.indexOf('Tram') >= 0;
  //transit.name = isTram ? 'Tram' : 'Bus';
  var transitSteps = steps
    .filter(function(step){
      return step.travel_mode == "TRANSIT";
    });
  var transitDistances = transitSteps
    .map(function(step){
      return step.distance.value;
    }).concat(0);
  var transitDistance = transitDistances
    .reduce(function(value1, value2){
      return value1 + value2;
    });

  var walkingSteps = steps
    .filter(function(step){
      return step.travel_mode == google.maps.TravelMode.WALKING;
    });
  var walkingDistances = walkingSteps
    .map(function(step){
      return step.duration.value;
    }).concat(0);
  var walkingDistance = walkingDistances
    .reduce(function(value1, value2){
      return value1 + value2;
    });

  var resultObj = {
    "key": transit.key,
    "type": transit.type,
    "price": Math.floor(transit.priceMultiplier*6.90),
    "calories": Math.floor(transit.caloriesMultiplier*walkingDistance),
    "emissions": Math.floor(transit.emissionsMultiplier*transitDistance),
    "duration": Math.floor(leg.duration.value / 60)
  }

  //console.log("TRANSIT RESULT:",resultObj);

  var results = Session.get("results");
  results.push(resultObj);
  Session.set("results",results);
}

var telOFunWalkingCallBack = function (response, status){
  console.log("telOFunWalkingCallBack");
  if (status != 'OK') {
    console.log("Callback of Distance matrix return with error", response);
    return;
  }

  response.type="WALKING";
  //console.log("response tel o fun",response);
  resultsToDraw[TelOFun.key].push(response);
  //console.log("resultsToDraw",resultsToDraw);
  let responseData = response.routes[0].legs[0];
  var resultObj = {
    "type": "WALKING",
    "duration": responseData.duration.value,
    "distance": responseData.distance.value
  }

  var results = Session.get("telofunresults");
  results.push(resultObj);
  Session.set("telofunresults",results);

  var counter = Session.get("telofuncounter") + 1;
  Session.set("telofuncounter",counter);
}

var telOFunBikingCallBack = function (response, status) {
  console.log("telOFunBikingCallBack");
  if (status != 'OK') {
    console.log("Callback of Distance matrix return with error", response);
    return;
  }
  response.type="BIKING";
  //console.log("response tel o fun",response);
  resultsToDraw["TEL-O-FUN"].push(response);
  //console.log("resultsToDraw",resultsToDraw);
  let responseData = response.routes[0].legs[0];
  var resultObj = {
    "type": "BIKING",
    "duration": responseData.duration.value / 3, //biking is 3 times faster
    "distance": responseData.distance.value
  }

  var results = Session.get("telofunresults");
  results.push(resultObj);
  Session.set("telofunresults",results);

  var counter = Session.get("telofuncounter") + 1;
  Session.set("telofuncounter",counter);
}


var trainTransitCallBack = function (response, status) {
  if (status != 'OK') {
    console.log("Callback of Distance matrix return with error", response);
    return;
  }

  //console.log("response tel o fun",response);
  resultsToDraw["TRAIN"].push(response);

  let responseData = response.routes[0].legs[0];
  var resultObj = {
    "type": "TRAIN",
    "duration": responseData.duration.value,
    "distance": responseData.distance.value
  }


  var steps = responseData.steps;

  //for each step filter the instructions to get the actual transportation method for each legs
  var travelMods = steps.filter(function(step){
      return step.instructions;
    })
    .map(function(step){
      return step.instructions.split(' ')[0];
    });


  trainIndex = travelMods.indexOf("Train");
  if (trainIndex == -1) {
    trainIndex = travelMods.indexOf("רכבת");
  }
  if (trainIndex == -1) {
    console.log("No train in transit results!");
    return;
  }

  var resultObj = {
    "type": responseData.steps[trainIndex].travel_mode,
    "duration": responseData.steps[trainIndex].duration.value,
    "distance": responseData.steps[trainIndex].distance.value
  }

  //console.log("TRAIN STEP: ",steps[trainIndex]);
  //console.log("resultObj",resultObj);

  var results = Session.get("trainresults");
  results.push(resultObj);
  Session.set("trainresults",results);

  var counter = Session.get("traincounter") + 1;
  Session.set("traincounter",counter);


}


var trainStationTransitCallBack = function (response, status) {
  if (status != 'OK') {
    console.log("Callback of Distance matrix return with error", response);
    return;
  }


  resultsToDraw["TRAIN"].push(response);

  let responseData = response.routes[0].legs[0];
  //console.log("TRAIN 1st/last leg response data",responseData);
  var resultObj = {
    "type": responseData.steps[0].travel_mode,
    "duration": responseData.duration.value,
    "distance": responseData.distance.value
  }

  var results = Session.get("trainresults");
  results.push(resultObj);
  Session.set("trainresults",results);

  var counter = Session.get("traincounter") + 1;
  Session.set("traincounter",counter);
}

Tracker.autorun(function() {
  console.log("GOT TO TRAIN AUTORUN");
  var counter = Session.get("traincounter");
  if (!train) return;
  //console.log("counter is:",counter);
  if (counter===3) {
    Tracker.nonreactive(function () {
      console.log("insert final train results");
      var results = Session.get("trainresults");

      //console.log("final TEL O FUN RESULT OBJECT", results);
      var walkingDistance = 0;
      var trainDistance = 0;
      var driveDistance = 0;
      var walkingDuration = 0;
      var trainDuration =0;
      var driveDuration = 0;


      results.forEach(function (item) {
        if (item.type==="WALKING") {
          walkingDistance += item.distance;
          walkingDuration += item.duration;
        }
        else if (item.type==="WALKING") {
          driveDistance += item.distance;
          driveDuration += item.duration;
        }
        else if (item.type === "TRANSIT") {
          trainDistance += item.distance;
          trainDuration += item.duration;
        }
      });


      var resultObj = {
        "key": "TRAIN",
        "type": "TRAIN",
        "price": Math.floor(train.priceMultiplier*25), //TODO: calculate price
        "calories": Math.floor(train.walkingCaloriesMultiplier*walkingDistance),
        "emissions": Math.floor(train.trainEmissionsMultiplier*trainDistance + train.drivingEmissionsMultiplier*driveDistance),
        "duration": Math.floor((walkingDuration + driveDuration + trainDuration)/60)
      }


      var results = Session.get("results");
      results.push(resultObj);
      Session.set("results",results);
    });

  }
});


/* Debug only: */
if (DEBUG_APP & Meteor.isClient) {
    Meteor.startup(function () {
        console.log("Add DEMO results for result template");
        var results = [
            {
                "type": "Driving",
                "icon": "/icons/car189.svg",
                "price": "100",
                "calories": "10",
                "emissions": "200",
                "duration": "50m"
            },
            {
                "type": "Bus",
                "icon": "/icons/bus46.svg",
                "price": "6.90",
                "calories": "35",
                "emissions": "150",
                "duration": "1hr 23m"
            },

        ];
        Session.set("results",results);
    });
}
