TransportationMethod = class TransportationMethod {
  /*
   * @origins: array of addresses as origins, 1 or more
   * @destinations: array of destinations, 1 or more
   * @destinationService: a Google Oboject that helps
   * */
  constructor (key, origins, destinations, directionsService, matrixService, type) {
    this.key = key;
    this.type = type;
    this.origins = origins;
    this.destinations = destinations;
    this.directionsService = directionsService; //TODO: remove?
    this.matrixService = matrixService;
    this.unitSystem = google.maps.UnitSystem.METRIC;
  }
  
  //call this to activate the Google Distance Matrix and get duration, length of
  activateMatrix(callback)  {
    this.matrixService.getDistanceMatrix({
      origins: [this.origins],
      destinations: [this.destinations],
      travelMode: this.type,
      unitSystem: this.unitSystem,
      durationInTraffic: false,
      avoidHighways: false,
      avoidTolls: false
    }, callback);
  };

  activateDirectionsService(callback, origin, destination) {
    if (!this.directionsService) {
      console.log("Directions Service is not initialized!");
      return;
    }

    let request = {
        origin: origin,
        destination: destination,
        travelMode: this.type
    }

    this.directionsService.route(request, callback);
  }
}



Driving = class Driving extends TransportationMethod {
  constructor (origins, destinations, directionsService, matrixService) {

    super("DRIVING", origins, destinations, directionsService, matrixService, google.maps.TravelMode.DRIVING);
    //overcome ES6 issues for the time being. TODO: conform to ES6 objects
    this.caloriesMultiplier = 0;
    this.priceMultiplier = 2.738/1000; //2.738 NIS / KM
    this.emissionsMultiplier = 271/1000; //271 gr. CO2 / KM

  }

  static get key() { return "DRIVING"; }
  static get display() {
      return {
        name: "Driving",
        icon: "/icons/transportation/driving_english.svg",
        verb: "drove by car",
        nounPlural: "car rides"
      };
  }
}


Taxi = class Taxi extends TransportationMethod {
  constructor (origins, destinations, directionsService, matrixService) {

    super("TAXI", origins, destinations, directionsService, matrixService, google.maps.TravelMode.DRIVING);

    this.caloriesMultiplier = 0;
    this.priceMultiplier = 1; //To be calculated differently
    this.emissionsMultiplier = 201/1000; //271 gr. CO2 / KM

  }
  
  static get key() { return "TAXI"; }
  static get display() {
      return {
          name: "Taxi",
          icon: "/icons/transportation/taxi_english.svg",
          verb: "took a taxi",
          nounPlural: "taxi rides"
      };
  }
}


Biking = class Biking extends TransportationMethod {
  constructor (origins, destinations, directionsService, matrixService) {

    super("BIKING", origins, destinations, directionsService, matrixService, google.maps.TravelMode.WALKING);
    //overcome ES6 issues for the time being. TODO: conform to ES6 objects
    this.caloriesMultiplier = 9.5 / 60 / 3; //calories / minute (and /3 is because time calculated is for walking)
    this.priceMultiplier = 0;
    this.emissionsMultiplier = 5/1000; //5 gr. CO2 / KM

  }

  static get key() { return "BIKING"; }
  static get display() {
      return {
          name: "Cycling",
          icon: "/icons/transportation/personalbike_english.svg",
          verb: "rode a bike",
          nounPlural: "bike rides"
      };
  }
}


Walking = class Walking extends TransportationMethod {
  constructor (origins, destinations, directionsService, matrixService) {

    super("WALKING", origins, destinations, directionsService, matrixService, google.maps.TravelMode.WALKING);
    //overcome ES6 issues for the time being. TODO: conform to ES6 objects
    this.caloriesMultiplier = 4.4 / 60; //calories / minute (calculate from time)
    this.priceMultiplier = 0;
    this.emissionsMultiplier = 0;

  }

  static get key() { return "WALKING"; }
  static get display() {
      return {
          name: "Walking",
          icon: "/icons/transportation/walking_english.svg",
          verb: "walked",
          nounPlural: "walks"
      };
  }
}

Bus = class Bus extends TransportationMethod {
  constructor (origins, destinations, directionsService, matrixService) {
    super("BUS", origins, destinations, directionsService, matrixService, google.maps.TravelMode.TRANSIT);
    //overcome ES6 issues for the time being. TODO: conform to ES6 objects
    this.caloriesMultiplier = 4.4 / 60; //same as the walking calories (to/from the station)
    this.priceMultiplier = 1; //price is 6.90, fixed)
    this.emissionsMultiplier = 101/1000; //101 gr. CO2 / KM
  }
    static get key() { return "BUS"; }
    static get display() {
        return {
            name: "Bus",
            icon: "/icons/transportation/bus_english.svg",
            verb: "took a bus",
            nounPlural: "bus rides"
        };
    }
}

TelOFun = class TelOFun extends TransportationMethod {
  constructor (origins, destinations, directionsService, matrixService) {
    super("TEL-O-FUN", origins, destinations, directionsService, matrixService, google.maps.TravelMode.WALKING);
    //overcome ES6 issues for the time being. TODO: conform to ES6 objects
    this.bikingCaloriesMultiplier = 9.5 / 60 / 3; //calories / minute (and /3 is because time calculated is for walking)
    this.walkingCaloriesMultiplier = 4.4 / 60; //calories / minute (calculate from time)
    this.priceMultiplier = 1;
    this.emissionsMultiplier = 4/1000; //4 gr. CO2 / KM
  }

  activateDirectionsService(telOFunWalkingCallBack, telOFunBikingCallBack, origin, destination) {
    if (!this.directionsService) {
      console.log("Directions Service is not initialized!");
      return;
    }

    //console.log("directions_origin",origin);

    var stationNearStart = getNearestTelOFunStation(origin.lat(),origin.lng());
    var stationNearEnd = getNearestTelOFunStation(destination.lat(),destination.lng());

    //determine method - if distance from start/end point to tel-o-fun station < 3 km , offer the option of Tel O Fun
    let firstLegDistance = calcDistance(origin.lat(),origin.lng(),stationNearStart.lat,stationNearStart.lng);
    let lastLegDistance = calcDistance(destination.lat(),destination.lng(),stationNearEnd.lat,stationNearEnd.lng);

    //console.log("Distances:",firstLegDistance, lastLegDistance);
    if (firstLegDistance > 3|| lastLegDistance > 3) {
      console.log("distance to/from Tel-O-Fun stations is too long, not displaying Tel-O-Fun result");
      return;
    }

    else {

      var startStationLatLng = new google.maps.LatLng(stationNearStart.lat, stationNearStart.lng);
      var endStationLatLng = new google.maps.LatLng(stationNearEnd.lat, stationNearEnd.lng);

      let firstLegRequest = {
        origin: origin,
        destination: startStationLatLng,
        travelMode: this.type //walking
      };

      let secondLegRequest = {
        origin: startStationLatLng,
        destination: endStationLatLng,
        travelMode: this.type //walking - NOT bike, since google's algorithm doesn't support bikes, we are approximating
      };

      let thirdLegRequest = {
        origin: endStationLatLng,
        destination: destination,
        travelMode: this.type //walking
      };

      this.directionsService.route(firstLegRequest, telOFunWalkingCallBack);
      this.directionsService.route(secondLegRequest, telOFunBikingCallBack);
      this.directionsService.route(thirdLegRequest, telOFunWalkingCallBack);



      //this.directionsService.route(request, callback);
    }
  }

  static get key() { return "TEL-O-FUN"; }
  static get display() {
      return {
          name: "Tel-O-Fun",
          icon: "/icons/transportation/personalbike_english.svg",
          verb: "rode Tel-O-Fun bikes",
          nounPlural: "Tel-O-Fun bike rides"
      };
  }

}



Train = class Train extends TransportationMethod {
  constructor (origins, destinations, directionsService, matrixService) {
    super("TRAIN", origins, destinations, directionsService, matrixService, google.maps.TravelMode.TRANSIT);
    //overcome ES6 issues for the time being. TODO: conform to ES6 objects
    this.walkingCaloriesMultiplier = 4.4 / 60; //calories / minute (calculate from time)
    this.trainCaloriesMultiplier = 0;
    this.drivingCaloriesMultiplier = 0;
    this.priceMultiplier = 1;
    this.trainEmissionsMultiplier = 6.1/1000; //6.1 gr. CO2 / KM - source: http://www.atoc.org/clientfiles/files/publicationsdocuments/npsB3A7_tmp.pdf
    this.drivingEmissionsMultiplier = 271/1000;

  }

  activateDirectionsService(trainTransitCallBack, trainStationTransitCallBack, origin, destination) {
    if (!this.directionsService) {
      console.log("Directions Service is not initialized!");
      return;
    }

    //console.log("directions_origin",origin);

    var stationNearStart = getNearestTrainStation(origin.lat(),origin.lng());
    var stationNearEnd = getNearestTrainStation(destination.lat(),destination.lng());

    //console.log("train: stationNearStart",stationNearStart);
    //console.log("train: stationNearEnd",stationNearEnd);

    //determine method - if distance from start/end point to tel-o-fun station < 3 km , offer the option of Tel O Fun
    let firstLegDistance = calcDistance(origin.lat(),origin.lng(),stationNearStart.lat,stationNearStart.lng);
    let lastLegDistance = calcDistance(destination.lat(),destination.lng(),stationNearEnd.lat,stationNearEnd.lng);

    //console.log("Distances:",firstLegDistance, lastLegDistance);

    if (firstLegDistance > 20 || lastLegDistance > 20) {
      console.log("distance to/from train stations is too long, not displaying train result");
      return;
    }

    else {

      var startStationLatLng = new google.maps.LatLng(stationNearStart.lat, stationNearStart.lng);
      var endStationLatLng = new google.maps.LatLng(stationNearEnd.lat, stationNearEnd.lng);




      //make first leg of the travel driving or walking depending on distance
      let firstLegRequest = {
        origin: origin,
        destination: startStationLatLng,
        travelMode: (firstLegDistance > 3) ? google.maps.TravelMode.DRIVING : google.maps.TravelMode.WALKING
      };

      let secondLegRequest = {
        origin: startStationLatLng,
        destination: endStationLatLng,
        travelMode: this.type //TRANSIT
      };


      let thirdLegRequest = {
        origin: endStationLatLng,
        destination: destination,
        travelMode: google.maps.TravelMode.WALKING
      };

      this.directionsService.route(firstLegRequest, trainStationTransitCallBack);
      this.directionsService.route(secondLegRequest,trainTransitCallBack);
      this.directionsService.route(thirdLegRequest, trainStationTransitCallBack);

    }
  }
  static get key() { return "TRAIN"; }
  static get display() {
      return {
          name: "Train",
          icon: "/icons/transportation/train1.png",
          verb: "took",
          nounPlural: "train rides"
      };
  }


}

var transportationMethodClasses = [Driving, Taxi, Biking, Walking, Bus, TelOFun, Train];
transportationMethods = {};
transportationMethodClasses.forEach(function(cls) {
    transportationMethods[cls.key] = cls;
});