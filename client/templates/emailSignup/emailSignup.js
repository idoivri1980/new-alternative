Template.emailSignup.helpers({
  error: function () {
    return Session.get('signupError');
  }
});

Template.emailSignup.onCreated(function() {
  Session.set('signupError', null);
});

Template.emailSignup.events({
  'submit form': function(evt) {
    evt.preventDefault();
    var user = {
      email: $('#email').val(),
      password: $('#password').val()
    };
    Session.set('signupError', null);
    Accounts.createUser(user, function(error) {
      if (error) {
        Session.set('signupError', error.reason);
      } else {
        Meteor.loginWithPassword(user.email, user.password, function(error) {
          if (error) {
            Session.set('loginError', 'Signed up successfully, but encountered an error while logging in: ' + error.reason);
          } else {
            Router.go('navigate');
          }
        });
      }
    });
  }
});