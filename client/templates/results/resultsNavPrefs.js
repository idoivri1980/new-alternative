Template.resultsNavPrefs.helpers({
    prefActiveClass: function(prefName) {
        var curPrefName = Session.get("preference");
        return (prefName === curPrefName) ? 'active' : '';
    }
});

Template.resultsNavPrefs.events({
    'click .nav-prefs a': function(evt) {
        Session.set('preference', $(evt.currentTarget).attr('data-pref'));
    }
});