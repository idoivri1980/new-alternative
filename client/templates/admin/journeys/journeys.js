Template.journeys.created = function () {
    this.subscribe('journeys');
    this.subscribe('users');
};

Template.journeys.helpers({
    userName: function () {
        var userId = Router.current().params._id;
        var user = Meteor.users.findOne({_id: userId});
        if (user) {
            return user.displayName();
        }
        return '';
    },
    journeys: function () {
        var userId = Router.current().params._id;
        return Journeys.find({userId: userId}, {sort: {'createdAt': -1}});
    }
});