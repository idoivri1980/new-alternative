Router.configure({
    layoutTemplate: 'appLayout',
    trackPageView: true
});

if (Meteor.isClient) {
    Router.plugin('seo', {
      only: ['pubJourney']
    });
}

Router.route('/', {
  name: 'login'
});

Router.route('/login', {
  name: 'emailLogin',
  parent: 'login'
});

Router.route('/signup', {
  name: 'emailSignup',
  parent: 'login'
});

Router.route('/profile', {
  name: 'profile'
});

Router.route('/navigate', {
  name: 'navigate'
});

Router.route('/about', {
  name: 'about',
  parent: 'navigate'
});

Router.route('/results', {
  name: 'results',
  parent: 'navigate'
});

Router.route('/map', {
  name: 'map',
  parent: 'results'
});

Router.route('/tracking', {
  name: 'tracking'
});

Router.route('/journey/:_id', {
  name: 'pubJourney',
  data: function () {
    return Journeys.findOne({_id: this.params._id});
  },
  seo: {
    title: function () {
      if (Session.get("pubJourney")) {
        return "I " + transportationMethods[Session.get("pubJourney").chosenType].display.verb;
      }
      return "";
    },
    description: function () {
      if (Session.get("pubJourney")) {
        var journey = Session.get("pubJourney");
          var formatAddress = function (address) {
              return address.replace(', Israel', '').replace(', ישראל', '');
          };
        return "I " + transportationMethods[journey.chosenType].display.verb +
            " from " + formatAddress(journey.origin) + " to " + formatAddress(journey.destination);
      }
      return "";
    },
    image: function () {
        return Meteor.absoluteUrl('/images/meta.png');
    }
  }
});

Router.route('/admin/users', {
  name: 'users'
});

Router.route('/admin/users/:_id/journeys', {
  name: 'journeys',
  parent: 'users'
});

Router.route('/admin/users/:_userId/journeys/:_journeyId/tracks', {
  name: 'tracks',
  parent: 'users'
});

Router.route('/admin/users/:_id/change-password', {
    name: 'changePassword',
    parent: 'users'
});

Router.route('/admin/notifications', {
  name: 'notifications'
});

Router.route('/admin/export', {
  name: 'export'
});

Router.route('/admin/stats', {
    name: 'stats'
});

var OnBeforeActions;
OnBeforeActions = {
    loginRequired: function(pause) {
        if (!Meteor.userId()) {
            Router.go('/');
        } else {
            this.next();
        }
    },
    loggedOutOnly: function(pause) {
        if (Meteor.userId()) {
            Router.go('navigate');
        } else {
            this.next();
        }
    }
};
Router.onBeforeAction(OnBeforeActions.loginRequired, {
    only: ['profile', 'navigate', 'results', 'map', 'users', 'export', 'notifications', 'tracking']
});
Router.onBeforeAction(OnBeforeActions.loggedOutOnly, {
    only: ['emailLogin', 'emailSignup']
});

Router.route('/api/tracks', {where: 'server'})
        .post(function() {
            console.log('got a track');
            // console.log(this.request.body);
            var trackData = this.request.body;
            var trackId = Tracks.insert(trackData);
            this.response.end('danke!');
        });

Router.route('/api/export', { where: 'server' })
        .get(function() {
            console.log('export please');
            //console.log(this.request.cookies);
            var resp = this.response;
            Meteor.call('export', function(err, result) {
                if (err) {
                    console.log(err);
                    return;
                }
                resp.end(JSON.stringify(result));
            });
        });