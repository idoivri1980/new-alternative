Template.export.helpers({
    exportResult: function() {
        return Session.get('exportResult');
    }
});

Template.export.onCreated(function() {
    Meteor.call('export', function(err, result) {
        if (err) {
            Session.set('exportResult', 'Error: ' + err);
            return;
        }
        Session.set('exportResult', JSON.stringify(result));
    });
});