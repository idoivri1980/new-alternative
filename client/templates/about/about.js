Template.about.events({
  'click a[target=_blank]': function (event) {
    event.preventDefault();
    window.open(event.currentTarget.href, '_blank');
  }
});