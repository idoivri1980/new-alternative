Meteor.users.helpers({
  displayName: function () {
    var user = this;
    if (user.profile && user.profile.name) return user.profile.name;
    if (user.emails && user.emails.length) return user.emails[0].address;
    return user._id;
  }
});