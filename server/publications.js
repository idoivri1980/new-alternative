Meteor.publish('tracks', function() {
    return Tracks.find();
});

Meteor.publish('journeys', function() {
    if (this.userId) {
        var user = Meteor.users.findOne(this.userId);
        if (SimplePermissions.isAdmin(user))
            return Journeys.find();
    }
});

Meteor.publish('journey', function(id) {
    if (this.userId && SimplePermissions.isAdmin(Meteor.users.findOne(this.userId))) {
        return Journeys.find({_id: id});
    }
});

//TODO: note that this is EXACTLY the same as above but without permissions, should we consolidate?
Meteor.publish('journeys-by-user', function() {
        return Journeys.find();
});

Meteor.publish('users', function() {
    if (this.userId) {
        var user = Meteor.users.findOne(this.userId);
        if (SimplePermissions.isAdmin(user))
            return Meteor.users.find();
    }
    return;
});

Meteor.publish('curUserProfile', function() {
    return Meteor.users.find({ _id: this.userId }, {
      fields: { 'services.facebook.id': true }
    });
});