/* 
 * Hard-coded permissions
 */

SimplePermissions = {
    _adminFacebookUserIds: [
        '10153259688077393',    // Amir
        '10153321251772393',    // Amir - production
        '10153170673032691',    // Ido
        '10152844426072691',    // Ido - production
        '10153847551378279'     // Eyal - production
    ],
    _adminEmails: [
        'amir.ozer@gmail.com',
        'dalyot@tx.technion.ac.il',
        'idoivri@gmail.com',
        'amir@zencity.io'
    ],
    isAdmin: function (user) {
        return user && (
                (user.services.facebook && this._adminFacebookUserIds.indexOf(user.services.facebook.id) > -1) ||
                (user.emails && this._adminEmails.indexOf(user.emails[0].address) > -1) ||
                (user.profile && (user.profile.name === 'Ido Ivri')) ||
                (user.profile && user.profile.isTest)
            );
    }
};

Meteor.methods({
    isAdmin: function () {
        return SimplePermissions.isAdmin(Meteor.user());
    }
});