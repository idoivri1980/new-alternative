var isUpdatingToCurrentLocation = false;

var resetLocations = function () {
    var blankLocation = {
        valid: false,
        address: "",
        lat: 0,
        lng: 0
    };
    Session.set("currentToLocation", blankLocation);
    Session.set("currentFromLocation", blankLocation);
    Session.set('filledInFromAddress', false);
    Session.set('filledInToAddress', false);
    Session.set('shouldResetLocations', false);
    updateToCurrentLocation();
};

Tracker.autorun(function() {
    if (Session.get('shouldResetLocations')) {
        resetLocations();
    }
});

//every autocomplete object is bound to a different field
var autocompleteFrom;
var autocompleteTo;

var alternativeSearchBounds;

Template.navigate.helpers({
  isLoading: function() {
      return !Session.get("navigateGoogleMapsInitialized");  
  },
    
  currentFromAddress: function() {
    var fromAddress = Session.get("currentFromLocation");
    if (fromAddress.valid) {
      return fromAddress.address;
    }
    else return "";

  },

  currentToAddress: function() {
    var toAddress = Session.get("currentToLocation");
    if (toAddress.valid) {
      return toAddress.address;
    }
    else return "";
  },
  
  isFilledIn: function (point) {
      var result = point === 'from'
        ? Session.get('filledInFromAddress')
        : Session.get('filledInToAddress');
      return result;// ? 'true' : 'false';
  },

  isUpdatingToCurrentLocation: function () {
      var result = Session.get('isUpdatingToCurrentLocation');
      return result;// ? 'true' : 'false';
  },

  shouldDisplayInputAddon: function (point) {
      return (point === 'from')
        ? Session.get('filledInFromAddress') || Session.get('isUpdatingToCurrentLocation')
        : Session.get('filledInToAddress');
  }

  //TODO: remove this / move to results!
  //fitIcon: function() {
  //  if (Session.get("preference")=="eco")
  //    {
  //      return "/icons/eco2_english.svg"
  //    }
  //  else {
  //      return "/icons/eco2_english_deactivate.svg"
  //  }
  //}
});

Template.navigate.events({
  
  'input #new-from-address': function(evt) {
      var currentFromLocation = Session.get('currentFromLocation');
      currentFromLocation.valid = false;
      Session.set('currentFromLocation', currentFromLocation);
      evt.target.classList.remove("err");
      Session.set('filledInFromAddress', $(evt.currentTarget).val() !== '');
  },

  'input #new-to-address': function(evt) {
      var currentToLocation = Session.get('currentToLocation');
      currentToLocation.valid = false;
      Session.set('currentToLocation', currentToLocation);
      evt.target.classList.remove("err");
      Session.set('filledInToAddress', $(evt.currentTarget).val() !== '');
  },
  
  'click .btn-clear': function(evt) {
      if ($(evt.currentTarget).attr('data-point') === 'from') {
          var currentFromLocation = Session.get('currentFromLocation');
          currentFromLocation.address = '';
          currentFromLocation.valid = false;
          Session.set("currentFromLocation", currentFromLocation);
          Session.set('filledInFromAddress', false);
          $('#new-from-address').val('').focus();
      } else if ($(evt.currentTarget).attr('data-point') === 'to') {
          var currentToLocation = Session.get('currentToLocation');
          currentToLocation.address = '';
          currentToLocation.valid = false;
          Session.set("currentToLocation", currentToLocation);
          Session.set('filledInToAddress', false);
          $('#new-to-address').val('').focus();
      }
  },

  'click .btn-abort-detect': function(evt) {
      setIsUpdatingToCurrentLocation(false);
  },
  
  "click .btn-pref": function (evt) {
    Session.set("preference", $(evt.currentTarget).attr('data-pref'));
    validate();

    checkAddressesAndContinue();
  },
  
  "focusout input": function () {
    // fix fixed header on iOS. TODO: activate on iOS only?
    setTimeout(function() {
        window.scrollTo(document.body.scrollLeft, document.body.scrollTop);
    }, 0);
  }
});

Template.navigate.onCreated(function() {
  var currentFromLocation = Session.get('currentFromLocation');
  var currentToLocation = Session.get('currentToLocation');
  if (!currentFromLocation || !currentToLocation ||
      Session.get("shouldResetLocations") || Session.get("hasChosenRoute")) {
    resetLocations();
  }
  Session.set("hasChosenRoute", false);
});

var googleMapsLoaded = false;

Template.navigate.onRendered(function () {

    resultsToDraw = {
      "DRIVING": [],
      "WALKING": [],
      "BIKING": [],
      "BUS": [],
      "TEL-O-FUN": [],
      "TRAIN": [],
      "TAXI": []
    };
  Session.set("results",[]);
  Session.set("recalculateResults",true);

  //set an empty set of results for navigating
    Session.set("results",[]);
    Session.set("navigateGoogleMapsInitialized", false);
    
    setIsUpdatingToCurrentLocation(isUpdatingToCurrentLocation); // refresh UI
    var onGoogleMapsLoaded = function () {
        if (Session.get("navigateGoogleMapsInitialized")) return;
        var currentFromLocation = Session.get('currentFromLocation');
        if (currentFromLocation.address === '') {
            updateToCurrentLocation();
        }

        alternativeSearchBounds = _getBounds();

        // fix FastClick breaking autocomplete on iOS
        // by https://gist.github.com/miguelmota/ce663990b7ca07f18972
        $(document).on({
            'DOMNodeInserted': function() {
                $('.pac-item, .pac-item span', this).addClass('needsclick');
            }
        }, '.pac-container');

        initAutocomplete(autocompleteFrom, 'new-from-address', 'currentFromLocation');
        initAutocomplete(autocompleteTo, 'new-to-address', 'currentToLocation');

        Session.set("navigateGoogleMapsInitialized", true);
    };
    Tracker.autorun(function () {
        if (GoogleMaps.loaded()) {
            onGoogleMapsLoaded();
        }
    });
});

var setIsUpdatingToCurrentLocation = function (value) {
    isUpdatingToCurrentLocation = value;
    Session.set('isUpdatingToCurrentLocation', value);
    $('#new-from-address')
            .prop('readonly', value)
            .attr('placeholder', value ? 'Detecting current location...' : 'From address...');
};

var updateToCurrentLocation = function() {
  if (isUpdatingToCurrentLocation) return;
  setIsUpdatingToCurrentLocation(true);
  var location = Session.get("currentFromLocation");
  if (location & location.valid) return; // don't geolocate if there's a location already

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      var geocoder = new google.maps.Geocoder;
      geocoder.geocode({'location': latlng}, function(results, status) {
          if (!isUpdatingToCurrentLocation) return; // user aborted
          var currentFromLocation = Session.get('currentFromLocation');
          if (status === google.maps.GeocoderStatus.OK) {
            if (results[0]) {
              
              currentFromLocation.address = results[0].formatted_address;
              currentFromLocation.lat = results[0].geometry.location.lat();
              currentFromLocation.lng = results[0].geometry.location.lng();
              currentFromLocation.valid = true;
              
              Session.set('filledInFromAddress', true);
            } else { // no results found
              currentFromLocation.valid = false;
            }
          } else { // geocoder failed
            console.log('Geocoder failed due to: ' + status);
            currentFromLocation.valid = false;
          }
          Session.set('currentFromLocation', currentFromLocation);
          setIsUpdatingToCurrentLocation(false);
        });
    }, function() { // getCurrentPosition failed
        setIsUpdatingToCurrentLocation(false);
    });
  } else { // no geolocation
    setIsUpdatingToCurrentLocation(false);
  }  
};

var initAutocomplete = function (autocomplete, inputElementId, locationSessionKey) {
    var location = Session.get(locationSessionKey);
    autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById(inputElementId)),
            {
                //types: ['geocode'],
                componentRestrictions: {country: 'il'}
            }
    );

    autocomplete.setBounds(alternativeSearchBounds);

    autocomplete.addListener('place_changed', function () {

        var place = autocomplete.getPlace();
        console.log("place", place);

        if (!place || !place.geometry) {
            console.log("location is invalid");
            location.valid = false;
            return;
        }

        location.address = getPlaceDisplayAddress(place);
        location.lat = place.geometry.location.lat();
        location.lng = place.geometry.location.lng();
        location.valid = true;
        document.getElementById(inputElementId).classList.remove('err');
        
        Session.set(locationSessionKey, location);
    });
};

var getPlaceDisplayAddress = function (place) {
    //console.log('place.name = ' + place.name);
    //console.log('place.formatted_address = ' + place.formatted_address);
    if (place.types.some(function(e){ return e === 'street_address'})) {
        return place.formatted_address;
    }
    else {
        //console.log('is a POI with no address');
        return place.name;
    }
};

var checkAddressesAndContinue = function() {
  var currentFromLocation = Session.get('currentFromLocation');
  var currentToLocation = Session.get('currentToLocation');
  if (currentFromLocation.valid && currentToLocation.valid) {
    Router.go("results");
  }
  else {
    console.log("not all addresses are valid");
  }
};

/*
* Error validation stuff
* */

function validate(){

    // get all the inputs that have the validate class in it
    // this requires the input to have a class name valled validate
    var validateElements = document.getElementsByClassName("validate");

    // Get all the inputs
    // This code is required because the output from 'document.getElementsByClassName' is not fit to the current needs
    if (DEBUG_APP) console.log("validateElements",validateElements); // Uncomment this line if you want to see what i mean
    var inputs = Array.prototype.filter.call(validateElements,         function(element){
        return element.nodeName === 'INPUT';
    });

    // Loop through the inputs to be validated
    
    var currentFromLocation = Session.get('currentFromLocation');
    var currentToLocation = Session.get('currentToLocation');

    for(var i=0; i < inputs.length; i ++ ){
        var input = inputs[i];
        var locationObj = input.id=="new-from-address"? currentFromLocation : currentToLocation;
        if(input.value.length == 0){

            // generic placeholder
            input.placeholder = "Please enter an address";
            // error class
            input.classList.add("err");
            break;
        }
        else if (!locationObj.valid) {
            input.classList.add("err");
            break;
        }
    }

}

//apply recommended bounds for Gush Dan to the autocomplete mechanism
var _getBounds = function () {

  var bounds = new google.maps.LatLngBounds(
            new google.maps.LatLng(gushDan["southWestBoundary"][0],gushDan["southWestBoundary"][1]),
            new google.maps.LatLng(gushDan["northEastBoundary"][0],gushDan["northEastBoundary"][1])
  );

  return bounds;
}



