Template.changePassword.helpers({
    /*userName: function () {
        var userId = Router.current().params._id;
        var user = Meteor.users.findOne({_id: userId});
        console.log('user', user);
        if (user) {
            return user.displayName();
        }
        return '';
    }*/
});

Template.changePassword.events({
    'submit form': function (evt) {
        evt.preventDefault();
        var userId = Router.current().params._id;
        var newPassword = $('#new-password').val();
        Meteor.call('changeUserPassword', userId, newPassword, function (err, result) {
            if (err) {
                alert('Error');
                return;
            }
            alert('Success');
        })
    }
});