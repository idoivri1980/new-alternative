Template.navBar.helpers({
    isAdmin: function () {
        return Session.get('isAdmin');
    },
    parent: function () {
        return Router.current().route.options.parent;
    },
    isActiveClass: function (routeName) {
        return Router.current().route.getName() === routeName ? 'active' : '';
    }
});

Template.navBar.rendered = function () {
    $(document).on('click', function () {
        if ($('#nav-drawer').hasClass('open')) {
            $('#nav-drawer').drawer('hide');
        }
    });
};

Template.navBar.events({
    'click span.navbar-brand': function() {
        if (Router.current().route.getName() === 'navigate') {
            Session.set("shouldResetLocations", true);
        }
    }
});